package database

import java.sql.*
import java.util.*
// DONE: write init{} block where db 'game' and table 'users' will be created if don't exist
// DONE: getJDBCConnection() can be in init{} too
// DONE: useGameDatabase() can be in init{} too
// DONE: errors to System.err.println()
// DONE: drop table method
// DONE: user_id is a UNIQUE KEY
// TODO: create DB 'game' and TABLE 'users' if they don't exist in init{}
// DONE: return statements check
class DatabaseManager(
        private var username: String? = "game_manager",
        private var password: String? = "789098777"){
    init {
        getJDBCConnection()
        useGameDatabase()
    }

    private var connection: Connection? = null

    /**
     * Sets [connection] variable to be connected to MySQL table
     * with [username] and [password].
     */
    private fun getJDBCConnection(){
        val authData = Properties()
        authData.put("user", username)
        authData.put("password", password)
        try{
            connection = DriverManager.getConnection(
                    "jdbc:mysql://" +
                            "127.0.0.1" +
                            ":3306/",
                    authData)
        }catch (sqle: SQLException){
            System.err.println(sqle.printStackTrace()) // SQL exceptions exactly
        }catch (exception: Exception){
            System.err.println(exception.printStackTrace())
        }
    }

    /**
     * Executes command. That means there's no return value to this command.
     * Only actions are performed with database/table/row/column.
     */
    private fun executeCommand(cmd: String){
        var command = cmd
        if(!command.contains(";")) {
            command += ";"
        }
        val queryStatement: Statement? = connection!!.createStatement()
        queryStatement!!.execute(command)
    }

    /**
     * Executes request. That means a return value exists to such command.
     */
    private fun executeRequest(cmd: String) : String {
        var command = cmd
        if(!command.contains(";")) {
            command += ";"
        }
        val queryStatement: Statement? = connection!!.createStatement()
        val reply: ResultSet? = queryStatement!!.executeQuery(command)
        var replyString = "\n"
        while (reply!!.next()) {
            replyString += when(command){
                "SHOW DATABASES;" -> reply.getString("Database") + "\n"
                "SELECT DATABASE();" -> reply.getString(1) + "\n"
                else -> {
                    reply.getString(1) + "\n"
                }
            }
//            if(command.contains("SHOW DATABASES")){
//                replyString += reply.getString("Database") + "\n"
//                command = ""
//                //println(reply.getString("Database"))
//            }
//            else if(command.contains("DATABASE()")){
//                replyString += reply.getString(1) + "\n"
//                //println(reply.getString(1))
//            }
//            else replyString += reply.getString(1) + "\n"
//            //println(reply.getString(1))
        }
        return  replyString //+ "\n"
    }

    /**
     * Chose database 'game'
     */
    private fun useGameDatabase() = executeCommand("USE game;")

    /**
     * Request to show all databases available
     */
    fun showDatabases() : String = executeRequest("SHOW DATABASES;")

    /**
     * Show the name of the current database
     */
    fun showCurrentDatabase() : String = executeRequest("SELECT DATABASE();")

    /**
     * Inserts value [data] by user's id [userId]
     * @param userId    is user's id, a value of 'user_id' column in 'game' database
     * @param data      is user's data, a value of 'data' column in 'game' database
     */
    fun insertValue(userId : String, data: String) = executeCommand(
               "INSERT INTO users (user_id,data) " +
                    "VALUES(\"$userId\", \"$data\") " +
                    "ON DUPLICATE KEY UPDATE data=\"$data\";")

    /**
     * Selects value of 'data' column for user with 'user_id' = [userId]
     * @param userId    is user's id, a value of 'user_id' column in 'game' database
     */
    fun selectDataById(userId: String) : String = executeRequest(
            "SELECT data FROM users WHERE user_id = '$userId';")

    /**
     * Drops table 'users'
     */
    fun dropTableUsers() = executeCommand("DROP TABLE IF EXISTS users;")
}