import database.DatabaseManager
import listener.Listener


fun main(args: Array<String>) {
    var listener = Listener()
    listener.listen()
}


/* DatabaseManager demo
//
//    //initialize Database Manager
//    val db = DatabaseManager() // works ok
//    // make a connection to MySQL Server
//    db.getJDBCConnection() // works ok
//    //    switch to 'game' database
//    db.useGameDatabase() // works ok

    // execute the query to read all Databases
//    db.showDatabases() // works ok
//    db.showCurrentDatabase() // works ok
//    db.insertValue("bitch", "fucking!!!!!") // works ok
//    db.selectDataById("bitch") // works ok
//    db.dropTableUsers() // works ok
*/