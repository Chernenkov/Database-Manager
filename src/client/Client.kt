package client

import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.InetAddress
import java.net.Socket
import java.net.SocketException
import java.sql.SQLException

class Client(addr: String = "127.0.0.1", prt: Int = 31337): Thread() {
    private var socketClient: Socket = Socket()
    private var sendCommand: String
    private var receiveDBReply: String = ""
    private val port: Int = prt // init{} ?
    private val address: String = addr // init {} ?
    init{
        socketClient = Socket(InetAddress.getByName(address), port)
        sendCommand = ""
    }

//    fun connectClient(){
//
//    }
    /**
     * Interaction mode. Launches [Client] and listens for [listener.Listener]'s socket.
     */
    override fun run(){
        socketClient = Socket(InetAddress.getByName(address), port)
        println("Connection established: ${socketClient.isConnected}")
        var outToServer: DataOutputStream
        var inFromServer: DataInputStream
        while(socketClient.isConnected) {
            // SEND
            outToServer = DataOutputStream(socketClient.getOutputStream())
            inFromServer = DataInputStream(socketClient.getInputStream())
            sendCommand = readLine()!!
            outToServer.writeUTF(sendCommand)
            if(sendCommand == "@quit") {
                println("[DISCONNECTED FROM SERVER ${socketClient.inetAddress}]")
                disconnect()
                break
            }
            // RECEIVE
            println("REPLY: ${(inFromServer.readUTF())}")
        }
    }

    private fun disconnect() = socketClient.close()

    // DONE: get(usr) : data
    // TODO: set(usr, data) : Boolean
    /**
     * No-interaction mode. Function just makes single request and gets reply.
     * @param usr   User's name, for which you want to get data from db.
     * @return      Data for [usr]
     */
    fun get(usr: String) : String {
        if(!socketClient.isConnected) println("reconnection neededGET")//socketClient = Socket(InetAddress.getByName(address), port)
        receiveDBReply = ""
        val outToServer = DataOutputStream(socketClient.getOutputStream())
        val inFromServer = DataInputStream(socketClient.getInputStream())
        sendCommand = "@get$usr"
        outToServer.writeUTF(sendCommand)
        receiveDBReply = inFromServer.readUTF()
        //disconnect()
        return receiveDBReply
    }
    //
    fun set(usr: String, data: String) {
        if(!socketClient.isConnected) println("reconnection neededSET")//socketClient = Socket(InetAddress.getByName(address), port)
        receiveDBReply = ""
        val outToServer = DataOutputStream(socketClient.getOutputStream())
        sendCommand = "@set$usr[$data]"
        outToServer.writeUTF(sendCommand)
        //disconnect()
    }
}