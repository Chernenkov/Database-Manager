package listener

import database.DatabaseManager
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException

class Listener (prt: Int = 31337) : Thread() {
    private val port = prt
    private val database: DatabaseManager = DatabaseManager("game_manager","789098777")
    private var socketServer = ServerSocket(port, 0, InetAddress.getByName("127.0.0.1"))
    private var socketConnection = Socket()
    private var receivedCommand = ""
    private var sendMessage = "[TEST]"
    override fun run() {
        println("Server is listening: ${socketServer.isBound && !socketServer.isClosed}")
        var inFromClient: DataInputStream
        var outToClient: DataOutputStream
        socketConnection = socketServer.accept()
        while(socketServer.isBound && !socketServer.isClosed) {
            sendMessage = ""
            receivedCommand = ""
            //receive
            inFromClient = DataInputStream(socketConnection.getInputStream())
            receivedCommand = inFromClient.readUTF()
//            try{
//                inFromClient = DataInputStream(socketConnection.getInputStream())
//                receivedCommand = inFromClient.readUTF()
//            }catch (sockexc: SocketException){
//                socketConnection = socketServer.accept()
//                inFromClient = DataInputStream(socketConnection.getInputStream())
//                receivedCommand = inFromClient.readUTF()
//            }
            println("RECEIVED: $receivedCommand")
            when(receivedCommand){
                "@quit" -> {disconnect()}
                "@currentdb" -> {sendMessage = database.showCurrentDatabase()}
                "@alldbs" -> {sendMessage = database.showDatabases()}
                else -> {
                    if(receivedCommand.contains("@get") && receivedCommand.length > 4){ // if command is '@get' and user is defined in it
                        println(receivedCommand.substring(4, receivedCommand.length))
                        sendMessage = database.selectDataById(receivedCommand.substring(4, receivedCommand.length))
                    }else if(receivedCommand.contains("@set") && receivedCommand.length > 4){
                        val user = receivedCommand.substring(4, receivedCommand.indexOf('['))
                        val data = receivedCommand.substring((user.length + 5), receivedCommand.length - 1)
                        println("[DEBUG]\nuserlen = ${user.length} : $user\ndata = ${data.length} : $data")
                        database.insertValue(user, data)
                    }else{
                        sendMessage = "[ERROR] Wrong command: $receivedCommand"
                    }
                }
            }
            //send
            println(database.selectDataById(receivedCommand.slice(0 until receivedCommand.length-1)))
            if(receivedCommand == "@quit") break // because when-expression doesn't handle break and continue
            outToClient = DataOutputStream(socketConnection.getOutputStream())
            outToClient.writeUTF(sendMessage)
        }
        disconnect()
        println("[DISCONNECTED]")
    }

    fun listen() = start()

    private fun disconnect() {
        socketConnection.close()
        socketServer.close()
    }
}
//
// TODO: Listener API:
// DONE: @currentdb
// DONE: @alldbs
// DONE: @get%username%
// TODO: @set%username%[%data%]
//